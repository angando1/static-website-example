# --------------- DÉBUT COUCHE OS -------------------
FROM nginx
LABEL version="1.0" maintainer="NGANDO Armel <ngandoarmel@yahoo.fr>"
COPY ./assets /usr/share/nginx/html/assets
COPY ./error /usr/share/nginx/html/error
COPY ./images /usr/share/nginx/html/images
COPY index.html /usr/share/nginx/html
COPY default.conf /etc/nginx/conf.d/default.conf
ENV PORT=8080
EXPOSE 8080

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'

##VOLUME nginx.conf /etc/nginx/nginx.conf


